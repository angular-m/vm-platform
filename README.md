# DevSecOps - VM-Platform - Plataforma de Gestión de Vulnerabilidades (SCA)

## Descripción
Bienvenido al repositorio de la solución DevSecOps - VM-Platform para la gestión de vulnerabilidades en el ciclo de vida del software. Nuestra misión es ir más allá de simplemente informar sobre vulnerabilidades; aspiramos a mejorar la eficiencia en la asignación de recursos, reducir riesgos y optimizar los tiempos de remediación.

## Objetivo
El propósito de este proyecto es crear una solución que facilite la representación visual de las vulnerabilidades informadas durante el análisis de dependencias (SCA) de una aplicación. La solución se encargará de recuperar el informe generado por la herramienta Snyk a través de su API mediante un microservicio designado como backend. Posteriormente, estos datos se presentarán de manera accesible en un panel de control.

## Características

- Análisis de dependencias (SCA) de una aplicación.
- Consulta de reportes de vulnerabilidades desde un microservicio.
- Visualización de la información de los reportes.

## Instalación Backend

1. Clona este repositorio en tu máquina local usando `git clone https://gitlab.com/nestjs4/vulnerabilitymanagementplatform.git`.
2. Navega a la carpeta del proyecto usando `cd proyecto`.
3. Crea un archivo con el nombre `.env` utilizando la plantilla `.temp.env`
4. Revisa la siguiente documentación para obtener `ORG_ID y TOKEN` y poder autenticarse con Snyk [Documentación](https://docs.snyk.io/getting-started/how-to-obtain-and-authenticate-with-your-snyk-api-token)
5. Instala las dependencias necesarias usando `npm install` (asegúrate de tener Node.js instalado en tu máquina)
6. Para iniciar el proyecto, usa el comando `npm run start:dev`. Esto iniciará el servidor y podrás acceder a la plataforma a través de tu navegador web en `http://localhost:3002/api/v1/projects`.
7. Revisar otros modos de iniciar el backend en el README del proyecto [Backend NestJs](https://gitlab.com/nestjs4/vulnerabilitymanagementplatform/-/blob/main/README.md?ref_type=heads)

## Instalación Frontend

1. Clona este repositorio en tu máquina local usando `git clone https://gitlab.com/angular-m/vulnerability-management-platform.git`.
2. Navega a la carpeta del proyecto usando `cd proyecto`.
3. Instala las dependencias necesarias usando `npm install` (asegúrate de tener Node.js instalado en tu máquina)
4. Para iniciar el proyecto, usa el comando `npm run start:proxy`. Esto iniciará el servidor y podrás acceder a la plataforma a través de tu navegador web en `http://localhost:4200/dashboard`.
5. Revisar otros modos de iniciar el frontend en el README del proyecto [Frontend Angular](https://gitlab.com/angular-m/vulnerability-management-platform/-/blob/master/README.md?ref_type=heads)

## Video

Puede ver un video de la app aquí 👉 [Ver el video](https://www.loom.com/share/1c6bcb5455e64cde850f3f4aa5c4fd0c?sid=232cab6e-85d0-49c8-96d0-dee1e5c48086)

 [![Ver el video](https://cdn.loom.com/sessions/thumbnails/1c6bcb5455e64cde850f3f4aa5c4fd0c-00001.jpg)](https://www.loom.com/share/1c6bcb5455e64cde850f3f4aa5c4fd0c?sid=232cab6e-85d0-49c8-96d0-dee1e5c48086)

<video width="320" height="240" controls>
  <source src="https://www.loom.com/share/1c6bcb5455e64cde850f3f4aa5c4fd0c?sid=232cab6e-85d0-49c8-96d0-dee1e5c48086" type="video/mp4">
</video>

## Demo

Puede ver una demo de la app aquí 👉 [Ir a la Demo](http://devsecopsvmplatform.s3-website-us-east-1.amazonaws.com/dashboard)

## Contribución

Si deseas contribuir a este proyecto, por favor realiza un "Fork" del repositorio y realiza tus cambios en esa copia. Una vez que estés listo para sugerir cambios, puedes hacer un "Pull Request" desde tu repositorio.

## Licencia

Este proyecto está licenciado bajo la Licencia MIT - vea el archivo LICENSE.md para más detalles.


